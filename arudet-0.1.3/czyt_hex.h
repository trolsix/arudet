/*
 * Copyright (C) 2020 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
#ifndef _czyt_hex_h
#define _czyt_hex_h

#include <stdint.h>

struct ALLMemory {
  uint32_t fsiz;
  uint32_t eesiz;
  uint8_t word;
  uint16_t fpagenmb;
  uint16_t fpagesiz;
  uint16_t eepagenmb;
  uint16_t eepagesiz;
  uint8_t * f_pages;   //is data
  uint8_t * ee_pages;   //is data
  uint8_t * f_memory;  //flash
  uint8_t * fr_memory;  //flash read
  uint8_t * fv_memory;  //flash to veryfi
  uint8_t * ee_memory;  //eeprom
  uint8_t * eer_memory;  //eeprom read
  //uint32_t fnmb;        //ilosc danych
};

uint32_t read_hex_c (
                FILE * pliknzw,
                uint32_t iled ,
                uint8_t * MEM ,
                uint32_t startadr,
                uint8_t * pageset,
                uint16_t pagesiz,
                uint8_t * isaction
);

uint32_t read_bin_a (
                FILE * pliknzw,
                uint32_t iled ,
                uint8_t * MEM ,
                uint32_t startadr,
                uint8_t * pageset,
                uint16_t pagesiz,
                uint8_t * isaction
);

unsigned char hextochar ( char * ws );

#endif

