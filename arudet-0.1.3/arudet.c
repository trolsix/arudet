/*
 * Copyright (C) 2020 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
/* -------------------------------- */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <sys/time.h>

#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>

/* -------------------------------- */

#include "czyt_hex.h"
#include "rsport.h"
#include "timeutil.h"
#include "hardw.h"

/* -------------------------------- */

char version[] = "0.1.3";

/* -------------------------------- */

void mosSIG ( uint32_t * );
void misSIG ( uint32_t * );
void sckSIG ( uint32_t * );
void rstSIG ( uint32_t * );
void pwrSIG ( uint32_t * );

void (* wsk_mos ) ( int , uint32_t * );
void (* wsk_mis ) ( int , uint32_t * );
void (* wsk_rst ) ( int , uint32_t * );
void (* wsk_sck ) ( int , uint32_t * );
void (* wsk_pwr ) ( int , uint32_t * );

void RTS_STAN ( int , uint32_t * );
void DTR_STAN ( int , uint32_t * );
void TXD_STAN ( int , uint32_t * );
void CTS_STAN ( int , uint32_t * );
void CTS_STAN_NEG ( int , uint32_t * );
void RI_STAN ( int , uint32_t * );
void RI_STAN_NEG ( int , uint32_t * );
void DSR_STAN ( int , uint32_t * );
void RTS_STAN_NEG ( int , uint32_t * );
void DTR_STAN_NEG ( int , uint32_t * );

int slij_32a ( uint32_t * odczyt , uint32_t * outy );
int slij_32b ( uint32_t * odczyt , uint32_t * outy );
int slij_32c ( uint32_t * odczyt , uint32_t * outy );
int (*send) ( uint32_t * odczyt , uint32_t * outy );

static uint8_t tryb2x = 0;
static uint8_t makeerase;
static uint8_t ignoreerasewhenflash;
static uint8_t ignoresignature;

static uint32_t delaybit = 10;
static uint32_t delayi = 0;
static char *pathcavrd = NULL;

static uint32_t adr;
static int32_t siz;

static uint32_t byteswillwrite;
static uint32_t writebytes;
						
/* ----------------------------------------- */

uint8_t readchpfcad ( char * pathcavrd, char *chips );
FILE * fileexistsopen( const char * filename );
int write_hex_a ( FILE * plik, uint32_t iled , uint8_t * MEM, uint8_t allm );
int write_bin_a ( FILE * plik, uint32_t iled , uint8_t * MEM );

/* -------------------------------- */

int wczytajstr ( char * a, char * b, size_t s );

/* RS232 DSUB9

1 DCD
2 Rx
3 Tx
4 DTR
5 Signal ground

6 DSR
7 RTS
8 CTS
9 RI

*/

/* -------------------------------- */
//  -U memtype:op:filename[:format]
/*
element
akcja
plik
format
devices
where
*/

char * memtype, *op, *filename, *format;

int udecode ( char * str ) {
	
	memtype = NULL;
	op = NULL;
	filename = NULL;
	format = NULL;
	
	if(str==NULL) return 0;
	if(str[0]<20) return 0;
	
	memtype = str;

	while( *str != ':') if(*++str==0) return 1;
	
	*str = 0;
	op = ++str;
	while( *str != ':') if(*++str==0) return 2;

	*str = 0;
	filename = ++str;
	while( *str != ':') if(*++str==0) return 3;

	*str = 0;
	format = ++str;
	
	return 4;
}

/* -------------------------------- */

int polecenia ( int argc, char *argv[] ) {
	
	int i = 0;
	int u = 0;
	
	makeerase = 0;
	ignoreerasewhenflash = 0;
	ignoresignature = 0;
	baudrateH = 9600;
	
	sprintf( hardwn , "UART1" );
	sprintf( wheren , "/dev/ttyS0" );

	desc[0] = 0;
	
	elementn = NULL;
	akcjan = NULL;
	memtype = NULL;
	op = NULL;
	filename = NULL;
	format = NULL;
	
	while ( ++i < argc ) {
	  //akcja
	  if ( strcmp ( argv[i], "-h" ) == 0 )  u = 1;
	  if ( strcmp ( argv[i], "-H" ) == 0 )  u = 1;
	  if (argv[1][0] == 'h' )  u = 1;
	  if (argv[1][0] == 'H' )  u = 1;

	  if (u) {
		 fprintf( stderr, "-p element\n" );
		 fprintf( stderr, "-U action <memtype>:r|w|v:<filename>:<format>\n" );
		 fprintf( stderr, "-c programmer\n" );
		 fprintf( stderr, "-P devices\n" );
		 fprintf( stderr, "-b baudrate port\n" );
		 fprintf( stderr, "-i delay bit-bang\n" );
		 fprintf( stderr, "-e erase chip\n" );
		 fprintf( stderr, "-F ignore signature\n" );
		 fprintf( stderr, "-D ignore erase when flash programming\n" );
		 fprintf( stderr, "-h help\n" );
		 fprintf( stderr, "-C config file\n" );
		 fprintf( stderr, "-x 2x clk - when clk is with TxD clk sometimes is faster\n" );
		 return 1;
	  }
	}

	for ( i=1; i<argc; ++i ) {

		//akcja
		if ( strcmp ( argv[i], "-U" ) == 0 ) {
			akcjan = argv[i+1];
			++i;
		}
		//erase
		if ( strcmp ( argv[i], "-e" ) == 0 ) {
		  makeerase = 1;
		}
		//element - 
		if ( strcmp ( argv[i], "-p"  ) == 0 ) {
		  elementn = argv[i+1];
			++i;
		}
		//where
		if ( strcmp ( argv[i], "-P" ) == 0 ) {
		  wczytajstr ( wheren, argv[i+1], sizeof(wheren) - 1 );
			++i;
		}
		//ignore erase before flash
		if ( strcmp ( argv[i], "-D" ) == 0 ) {
			ignoreerasewhenflash = 1;
		}
		//ignore signature
		if ( strcmp ( argv[i], "-F" ) == 0 ) {
			ignoresignature = 1;
		}
		//programer
		if ( strcmp ( argv[i], "-c" ) == 0 ) {
		  wczytajstr ( hardwn, argv[i+1], sizeof(hardwn) - 1 );
			++i;
		}
		//baud rs
		if ( strcmp ( argv[i], "-b" ) == 0 ) {
		  sscanf ( argv[i+1], "%u", &baudrateH );
			++i;
		}
		//delay i
		if ( strcmp ( argv[i], "-i" ) == 0 ) {
		  sscanf ( argv[i+1], "%u", &delayi );
			++i;
		}
		//path to config
		if ( strcmp ( argv[i], "-C" ) == 0 ) {
		  pathcavrd = argv[i+1];
			++i;
		}
		//tryb x2
		if ( strcmp ( argv[i], "-x" ) == 0 ) {
		  tryb2x = 1;
		}
		
		//ignore bad transmisji
		/*
		if ( strcmp ( argv[i], "-I" ) == 0 ) {
		  TEST_BAD = 0;
		}*/
	}
	
	udecode ( akcjan );
	return 0;
}

/* ----------------------------------------- */  

uint32_t fsiz = 0;
uint32_t fpagesiz = 0;
uint32_t eepagesiz = 0;
uint32_t eesiz = 0;
uint32_t fpagenmb = 0;
uint32_t eepagenmb = 0;

/* ----------------------------------------- */  

void * init( char * chip ) {
	
	struct ALLMemory * mem;
	
	mem = malloc(sizeof(*mem));
	if( mem == NULL ) return NULL;
	
	mem->fsiz = 0;
	mem->eesiz = 0;
	mem->fpagenmb = 0;
	mem->fpagesiz = 0;
	mem->eepagenmb = 0;
	mem->eepagesiz = 0;

	mem->ee_pages = NULL;
	mem->f_pages = NULL;
	mem->f_memory = NULL;
	mem->ee_memory = NULL;
	
	/*if ( strcmp( chip, "t15" ) == 0 ) {
	  mem->fsiz = 1024;
	  mem->word = 2;
	  mem->fpagenmb = 0;
	  mem->fpagesiz = 0;
	  mem->eepagenmb = 0;
     mem->eepagesiz = 0;
     mem->eesiz = 64;
     modepf = 0x04;
		signbyt[0]=0x1e;
		signbyt[1]=0x90;
		signbyt[2]=0x06;
	} else */if ( strcmp( chip, "t13" ) == 0 ) {
	  mem->fsiz = 1024;
	  mem->word = 2;
	  mem->fpagenmb = 32;
	  mem->fpagesiz = 32;
	  mem->eepagenmb = 16;
     mem->eepagesiz = 4;
     mem->eesiz = 64;
     modepf = 0x41;
		signbyt[0]=0x1e;
		signbyt[1]=0x90;
		signbyt[2]=0x07;
	} else if ( strcmp( chip, "m328" ) == 0 ) {
	  mem->fsiz = 4096*8;
	  mem->word = 2;
	  mem->fpagenmb = 256;
	  mem->fpagesiz = 128;
	  mem->eepagenmb = 256;
     mem->eepagesiz = 4;
     mem->eesiz = 1024;
     modepf = 0x41;
		signbyt[0]=0x1e;
		signbyt[1]=0x95;
		signbyt[2]=0x14;
	} else if ( strcmp( chip, "m328p" ) == 0 ) {
	  mem->fsiz = 4096*8;
	  mem->word = 2;
	  mem->fpagenmb = 256;
	  mem->fpagesiz = 128;
	  mem->eepagenmb = 256;
     mem->eepagesiz = 4;
     mem->eesiz = 1024;
     modepf = 0x41;
		signbyt[0]=0x1e;
		signbyt[1]=0x95;
		signbyt[2]=0x0F;
	} else if ( strcmp( chip, "m48" ) == 0 ) {
	  mem->fsiz = 4096;
	  mem->word = 2;
	  mem->fpagenmb = 64;
	  mem->fpagesiz = 64;
	  mem->eepagenmb = 64;
     mem->eepagesiz = 4;
     mem->eesiz = 256;
     modepf = 0x41;
		signbyt[0]=0x12;
		signbyt[1]=0x92;
		signbyt[2]=0x05;
	} else if ( strcmp( chip, "m8" ) == 0 ) {
	  mem->fsiz = 8192;
	  mem->word = 2;
	  mem->fpagenmb = 128;
	  mem->fpagesiz = 64;
	  mem->eepagenmb = 128;
     mem->eepagesiz = 4;
     mem->eesiz = 512;
     modepf = 0x41;
		signbyt[0]=0x1e;
		signbyt[1]=0x93;
		signbyt[2]=0x07;
	} else {
		readchpfcad ( pathcavrd, elementn );
		mem->fsiz = fsiz;
	  mem->word = 2;
	  mem->fpagenmb = fpagenmb;
	  mem->fpagesiz = fpagesiz;
	  mem->eepagenmb = eepagenmb;
     mem->eepagesiz = eepagesiz;
     mem->eesiz = eesiz;
		/*
	  mem->fsiz = 128;
	  mem->word = 2;
	  mem->fpagenmb = 1;
	  mem->fpagesiz = 128;
	  mem->eepagenmb = 0;
     mem->eepagesiz = 0;
     mem->eesiz = 64;
     modepf = 0x41;*/
		/*fprintf( stderr, "init elementn %s\n", chip );
		fprintf( stderr, "don't know %s controller", chip );
		fprintf( stderr, "used default\n");*/
	}

	if( mem->fsiz == 0 ){
		free(mem);
		return NULL;
	}
	
	if(mem->eepagenmb) mem->ee_pages = malloc( mem->eepagenmb );
	if(mem->fpagenmb) mem->f_pages = malloc( mem->fpagenmb );
	if(mem->fsiz) mem->f_memory = malloc( mem->fsiz );
	if(mem->eesiz) mem->ee_memory = malloc( mem->eesiz );
	if(mem->fsiz) mem->fr_memory = malloc( mem->fsiz );
	if(mem->eesiz) mem->eer_memory = malloc( mem->eesiz );
	if( mem->fsiz > mem->eesiz ) {
		if(mem->fsiz) mem->fv_memory = malloc( mem->fsiz );
	} else {
		if(mem->fsiz) mem->fv_memory = malloc( mem->eesiz );
	}


	memset( mem->f_pages , 0 , mem->fpagenmb );
	memset( mem->f_memory , 0xFF , mem->fsiz );
	memset( mem->ee_memory , 0xFF , mem->eesiz );
	memset( mem->fr_memory , 0xFF , mem->fsiz );
	memset( mem->eer_memory , 0xFF , mem->eesiz );
	
	if( mem->fsiz > mem->eesiz )
		memset( mem->fv_memory , 0x00 , mem->fsiz );
	else 
		memset( mem->fv_memory , 0x00 , mem->eesiz );

	return mem;
}

/* ----------------------------------------- */  
/*       0x0f polowa wypelnienia      */
/* ----------------------------------------- */
/* -U memtype:op:filename[:format] */

/*
eeprom       The EEPROM of the device.
efuse        The extended fuse byte.
flash        The flash ROM of the device.
fuse         The fuse byte in devices that have only a single fuse byte.
hfuse        The high fuse byte.
lfuse        The low fuse byte.
lock         The lock byte.
signature    The three device signature bytes (device ID).
*/

/*r w v */

/* filename */

/* i b */

/* ----------------------------------------- */  

int fd = -1;
uint32_t zero = 0;
uint32_t jeden = 1;

/* ----------------------------------------- */  

int myexit( int ret ){
	if( fd >= 0 ) {
		DTR_STAN ( fd , &zero );
		RTS_STAN ( fd , &zero );
		stanport ( wheren, 4, fd, 38400 );
	}
	exit(ret);
}

/* ----------------------------------------- */  

uint8_t readbyte( uint8_t *in, uint32_t dats32 ) {
	uint8_t t;
	uint32_t odczyt;
	
	send( &odczyt, &dats32 );
	*in = odczyt & 0xFF;
	t = ( odczyt >> 16 ) & 0xFF;
	t ^= dats32 >> 24;
	if ( t ) {
		fprintf( stderr, " bad: %02x ", 0xff & odczyt>>16 );
		return 1;
	}
	return 0;
}

/* ----------------------------------------- */

void writebytetofile( FILE * prgfile, uint8_t dat8 ) {
	if( (prgfile) && ( op[0] == 'r' ) ){
		if( format[0] == 'i' ) write_hex_a ( prgfile, 1 , &dat8, 1 );
		if( format[0] == 'r' ) write_bin_a ( prgfile, 1 , &dat8 );
	}
}

/* ----------------------------------------- */

uint32_t readfl ( char * str, uint8_t * dat8 ){
	
	if( str[0] != '0' ) return 0;
	if( ( str[1] != 'x' ) && ( str[1] != 'X' ) ) return 0;
	
	*dat8 = hextochar ( &str[2] );
	
	return 1;
}

/* ----------------------------------------- */

void readbytefromfile( FILE * prgfile, uint8_t * dat8 ) {
	uint32_t siz = 0;
	if( format[0] == 'i' ) siz = read_hex_c ( prgfile, 1, dat8, 0, 0, 0, NULL );
	if( format[0] == 'r' ) siz = read_bin_a ( prgfile, 1, dat8, 0, 0, 0, NULL );
	if( format[0] == 'm' ) siz = readfl ( filename, dat8  );
	if( 1 != siz ) {
		fprintf( stderr, "Bad read b : %02x\n", *dat8 );
		myexit(siz);
	}
}

/* ----------------------------------------- */

void showtime( void ) {
	uint32_t timems;
	
	timems = gettimems();
	timems *= byteswillwrite;
	if(writebytes) timems /= writebytes;
	
	timems /= 1000;
	fprintf( stderr, " %3u min", timems/60 );
	timems %= 60;
	fprintf( stderr, " %2u s\n", timems );
}

/* ----------------------------------------- */

void showtime3( void ) {
	uint32_t timems;
	
	timems = gettimems();
	timems *= siz;
	if(writebytes) timems /= writebytes;
	
	timems /= 1000;
	fprintf( stderr, " %3u min", timems/60 );
	timems %= 60;
	fprintf( stderr, " %2u s", timems );
}

/* ----------------------------------------- */
/*       read data eeprom from file           */

uint32_t rdeff( FILE * prgfile, struct ALLMemory *mem ){
	uint32_t adr = 0;
	if( format[0] == 'i' ) {
		adr = read_hex_c (
			prgfile,
			mem->eesiz,
			mem->ee_memory,
			0,
			mem->ee_pages,
			mem->eepagesiz,
			mem->fv_memory
		);
		if( 0==adr ) myexit(adr);
	
	}
	if( format[0] == 'r' ) {
		adr = read_bin_a (
			prgfile,
			mem->eesiz,
			mem->ee_memory,
			0,
			mem->ee_pages,
			mem->eepagesiz,
			mem->fv_memory
		);
		if( 0==adr ) myexit(adr);
	}
	return adr;
}

/* ----------------------------------------- */
/*       read data flash from file           */

uint32_t rdfff( FILE * prgfile, struct ALLMemory *mem ){
	uint32_t adr = 0;
	if( format[0] == 'i' ) {
		adr = read_hex_c (
			prgfile,
			mem->fsiz,
			mem->f_memory,
			0,
			mem->f_pages,
			mem->fpagesiz,
			mem->fv_memory
		);
		if( 0==adr ) myexit(adr);
	
	}
	if( format[0] == 'r' ) {
		adr = read_bin_a (
			prgfile,
			mem->fsiz,
			mem->f_memory,
			0,
			mem->f_pages,
			mem->fpagesiz,
			mem->fv_memory 
		);
		if( 0==adr ) myexit(adr);
	}
	return adr;
}

/* ----------------------------------------- */

int main ( int argc, char *argv[] ) {

	FILE * prgfile;
	struct ALLMemory * mem;
	uint8_t ndil;

	fprintf( stderr, "--------------------------------------\n");
	prgfile = NULL;
	
	if( polecenia ( argc, argv ) != 0 ) return 1;

	if( NULL == elementn ){
		fprintf( stderr, "choose option for -p\n" );
		return 1;
	}
	
	if( makeerase == 0 ) {
		if( NULL == akcjan ){
			fprintf( stderr, "choose option for -U\n" );
			return 1;
		}
		if( ( format[0] != 'm' ) && ( format[0] != 'i' ) && ( format[0] != 'r' )  ){
			fprintf( stderr, "don't support type format : %c\n", format[0] );
			fprintf( stderr, "choose i or b\n" );
			return 1;
		}
		fprintf( stderr, "mem type         : %s\n", memtype );
		fprintf( stderr, "what do          : %s\n", op );
		fprintf( stderr, "filename         : %s\n", filename );
		fprintf( stderr, "format           : %s\n", format );
		fprintf( stderr, "chip             : %s\n", elementn );
		fprintf( stderr, "hardwn           : %s\n", hardwn );
		fprintf( stderr, "port             : %s\n", wheren );
		fprintf( stderr, "delay            : %u us\n", delayi );
		fprintf( stderr, "baudrate         : %u kHz\n", baudrateH );
	} else {
		fprintf( stderr, "erase            : %s\n", elementn );
	}

	fprintf( stderr, "--------------------------------------\n");

	mem = init(elementn);
	byteswillwrite = 0;

	if( mem == NULL ) {
		fprintf( stderr, "don't find config chip: %s\n", elementn );
		return 1;
	}
	if(mem->fsiz == 0 ) {
		fprintf( stderr, "flash size 0\n" );
		return 1;
	}
	
	if( baudrateH < 300 ) {
		fprintf( stderr, "min baudarate 300... change\n" );
		baudrateH = 300;
	}
	
	fd = stanport ( wheren, 3, 0, baudrateH );
	if( fd == -1 ) return 1;
	
	/* pre start init */
	DTR_STAN ( fd , &zero );
	RTS_STAN ( fd , &zero );

	/* time min is 8/baudrate */
	
	if( delaybit < 10000000/baudrateH ) delaybit = 11000000/baudrateH;
	
	zero = 0;
	jeden = 1;
	
	/* for ts1programmer */
	if( strcmp ( hardwn, "TRSPR1" ) == 0 ) {
		wsk_rst = DTR_STAN_NEG;
		wsk_sck = TXD_STAN;
		wsk_mos = RTS_STAN;
		wsk_mis = CTS_STAN;
		send = slij_32b;
	}
	/* usb->uart 1 */
	else if( strcmp ( hardwn, "UART1" ) == 0 ) {
		wsk_rst = TXD_STAN;
		wsk_mos = RTS_STAN_NEG;
		wsk_sck = DTR_STAN_NEG;
		wsk_mis = CTS_STAN_NEG;
		send = slij_32a;
		if ( delayi ) delaybit = delayi;
	}
	/* usb->uart 2 */
	else if( strcmp ( hardwn, "UART2" ) == 0 ) {
		wsk_rst = DTR_STAN_NEG;
		wsk_mos = RTS_STAN_NEG;
		wsk_sck = TXD_STAN;
		wsk_mis = CTS_STAN_NEG;
		send = slij_32a;
	}
	/* usb->uart 3 */
	else if( strcmp ( hardwn, "UART3" ) == 0 ) {
		wsk_rst = TXD_STAN;
		wsk_mos = RTS_STAN;
		wsk_sck = DTR_STAN;
		wsk_mis = CTS_STAN;
		send = slij_32a;
	}
	/* usb->uart 4 */
	else if( strcmp ( hardwn, "UART4" ) == 0 ) {
		wsk_rst = DTR_STAN;
		wsk_mos = RTS_STAN;
		wsk_sck = TXD_STAN;
		wsk_mis = CTS_STAN;
		send = slij_32a;
	}
	/* RS232 OPTICAL 1 */
	else if( strcmp ( hardwn, "RSOPT1" ) == 0 ) {
		wsk_rst = TXD_STAN;
		wsk_mos = RTS_STAN_NEG;
		wsk_sck = DTR_STAN_NEG;
		wsk_mis = CTS_STAN_NEG;
		send = slij_32c;
		if ( delayi ) delaybit = delayi;
	}
	/* RS232 OPTICAL 2 */
	else if( strcmp ( hardwn, "RSOPT2" ) == 0 ) {
		wsk_rst = DTR_STAN_NEG;
		wsk_sck = TXD_STAN;
		wsk_mos = RTS_STAN_NEG;
		wsk_mis = CTS_STAN_NEG;
		send = slij_32c;
	} else {
		fprintf( stderr, "bad programmer :%s\n", hardwn );
		return 1;
	}
	
	if( tryb2x ) delaybit /= 2;
	fprintf( stderr, "delay            : %u us clk: %u Hz\n", delaybit, 500000/delaybit );
	
/* reset end program enable */
	mosSIG(&zero);
  sckSIG(&zero);
  rstSIG(&zero);
  usleep(20000);
  rstSIG(&jeden);
	usleep(delaybit);
  rstSIG(&zero);
  /* wait for enable 20ms */
  usleep(32000);

	uint32_t dats32, *dats32p;
	uint32_t odczyt;

	dats32p = &dats32;

	starttimer();
		
	/* enter programming */
	dats32 = 0xAC530000;
	send( &odczyt, dats32p );
	
	if( ( odczyt & ( 0xFF<<8 ) ) != ( 0x53<<8 ) ) {
		fprintf( stderr, "not enter in program operation %08x\n", odczyt );
		myexit( -2 );
	}
	  
	fprintf( stderr, "YES enter in program operation\n" );

	/* erase */
	if( makeerase ) {
		uint8_t dat8;
		fprintf( stderr, "erase chip\n" );
		dat8 = readbyte( &dat8, 0xAC800000 );
		usleep( 10000 );
		myexit( dat8 );
	}

	/* file opening */
	if( format[0] != 'm' ) {
		if( strcmp ( op, "r" ) == 0 ){
			prgfile = fileexistsopen( filename );
			if( prgfile == NULL ) {
				fprintf( stderr, "bad open %s file\n", filename );
				myexit(-3);
			}
		}
		if( strcmp ( op, "w" ) == 0 ){
			prgfile = fopen( filename, "r" );
			if( prgfile == NULL ) {
				fprintf( stderr, "bad open %s file\n", filename );
				myexit(-3);
			}
		}
		if( strcmp ( op, "v" ) == 0 ){
			prgfile = fopen( filename, "r" );
			if( prgfile == NULL ) {
				fprintf( stderr, "bad open %s file\n", filename );
				myexit(-3);
			}
		}
	}

	/* signature */
	if(1){
		uint8_t dat8[3];

		readbyte( &dat8[0], 0x30000000 );
		fprintf( stderr, "signature : %02x", dat8[0] );
		readbyte( &dat8[1], 0x30000100 );
		fprintf( stderr, " %02x", dat8[1] );
		readbyte( &dat8[2], 0x30000200 );
		fprintf( stderr, " %02x\n", dat8[2] );
		if(( dat8[0] != signbyt[0] ) ||
			( dat8[1] != signbyt[1] ) ||
			( dat8[2] != signbyt[2] ) ) {
				fprintf( stderr, "Bad signature..." );
				if( ignoresignature == 0 ) {
					fprintf( stderr, "\n" );
					myexit(3);
				}
				fprintf( stderr, " ignore\n" );
			}
	}
	
	/* lock */
	if( strcmp( memtype, "lock" ) == 0 ) {
		uint8_t dat8;
		if( op[0] == 'r' ) {
			readbyte( &dat8, 0x58000000 );
			fprintf( stderr, "lock      : 0x%02x\n", dat8 );
			writebytetofile( prgfile, dat8 );
		} else if( op[0] == 'w' ) {
			readbytefromfile( prgfile, &dat8 );
			fprintf( stderr, "lock writ : 0x%02x\n", dat8 );
			readbyte( &dat8, 0xACE00000 | dat8 );
		}
	}
	
	/* lfuse */
	if(( strcmp( memtype, "lfuse" ) == 0 ) ||
	   ( strcmp( memtype, "fuse" ) == 0 )	){
		uint8_t dat8;
		if( op[0] == 'r' ) {
			readbyte( &dat8, 0x50000000 );
			fprintf( stderr, "lfuse     : 0x%02x\n", dat8 );
			writebytetofile( prgfile, dat8 );
		} else if( op[0] == 'w' ) {
			readbytefromfile( prgfile, &dat8 );
			fprintf( stderr, "lfuse writ: 0x%02x\n", dat8 );
			readbyte( &dat8, 0xACA00000 | dat8 );
		}
	}

	/* hfuse */
	if( strcmp( memtype, "hfuse" ) == 0 ) {
		uint8_t dat8;
		if( op[0] == 'r' ) {
			readbyte( &dat8, 0x58080000 );
			fprintf( stderr, "hfuse     : 0x%02x\n", dat8 );
			writebytetofile( prgfile, dat8 );
		} else if( op[0] == 'w' ) {
			readbytefromfile( prgfile, &dat8 );
			fprintf( stderr, "hfuse writ: 0x%02x\n", dat8 );
			readbyte( &dat8, 0xACA80000 | dat8 );
		}
	}

	/* efuse */
	if( strcmp( memtype, "efuse" ) == 0 ) {
		uint8_t dat8;
		if( op[0] == 'r' ) {
			readbyte( &dat8, 0x50080000 );
			fprintf( stderr, "efuse     : 0x%02x\n", dat8 );
			writebytetofile( prgfile, dat8 );
		} else if( op[0] == 'w' ) {
			readbytefromfile( prgfile, &dat8 );
			fprintf( stderr, "efuse writ: 0x%02x\n", dat8 );
			readbyte( &dat8, 0xACA40000 | dat8 );
		}
	}

	starttimer();
	
	/* read flash */
	if(( strcmp( memtype, "flash" ) == 0 ) &&
		( strcmp( op, "r" ) == 0 ) ) {
		uint8_t dat8, *w8;

		fprintf( stderr, "read flash memory\n" );
		fprintf( stderr, "adr bytes: data  estimated time remain\n" );
			
		byteswillwrite = mem->fsiz / 2;

		writebytes = 0;
		ndil = 0x08 - 1;
		w8 = mem->fr_memory;
		while( byteswillwrite ) {
			
			if( 0 == ( writebytes & ndil ) )
				fprintf( stderr, "adr 0x%06x: ", writebytes*2 );
			/* low */
			readbyte( &dat8, 0x20000000 | (writebytes<<8) );
			*w8++ = dat8;
			fprintf( stderr, "%02x", dat8 );
			/* high */
			readbyte( &dat8, 0x28000000 | (writebytes<<8) );
			*w8++ = dat8;
			fprintf( stderr, "%02x", dat8 );
			writebytes++;
			
			if( 0 == ( writebytes & ndil ) ) showtime();
			if( 0 == --byteswillwrite ) break;
		}
		fprintf( stderr, "\n" );
		if( format[0] == 'i' )
			write_hex_a ( prgfile, mem->fsiz, mem->fr_memory, 0 );
		if( format[0] == 'r' )
			write_bin_a ( prgfile, mem->fsiz, mem->fr_memory );
	}

	/* veryfi flash */
	if(( strcmp( memtype, "flash" ) == 0 ) &&
	   ( strcmp( op, "v" ) == 0 ) ) {
		uint8_t dat8, *v8;

		fprintf( stderr, "veryfi flash memory\n" );
		fprintf( stderr, "adr bytes: data  estimated time remain\n" );
		
		siz = rdfff( prgfile, mem );
		if( siz > (int32_t)mem->fsiz ) siz = mem->fsiz;

		ndil = 0x10 - 1;
		v8 = mem->f_memory;
		
		for( writebytes=0, adr=0; adr<mem->fsiz; ) {
			
			if( 0 == ( adr & ndil ) ) fprintf( stderr, "\radr 0x%06x: ", adr );
			
			if( mem->fv_memory[adr+0] ) { /* low */
				readbyte( &dat8, 0x20000000 | (adr<<7) );
				if( dat8 != *v8 ) {
					fprintf( stderr, "bad data read: 0x%02x should be: 0x%02x", dat8, *v8 );
					fprintf( stderr, " at 0x%06x\n", adr );
					myexit(12);
				}
				++writebytes;
				if(siz) --siz;
			}
			++v8;
			if( mem->fv_memory[adr+1] ) { /* high */
				readbyte( &dat8, 0x28000000 | (adr<<7) );
				if( dat8 != *v8 ) {
					fprintf( stderr, "bad data read: 0x%02x should be: 0x%02x", dat8, *v8 );
					fprintf( stderr, " at 0x%06x\n", adr + 1 );
					myexit(12);
				}
				++writebytes;
				if(siz) --siz;
			}
			++v8;
			
			adr += 2;
			if( 0 == ( adr & ndil ) ) showtime3();
			if( 0 == siz ) break;
		}
		fprintf( stderr, "\nveryfi OK\n" );
	}
	
	/* write flash */
	if(( strcmp( memtype, "flash" ) == 0 ) &&
		( strcmp( op, "w" ) == 0 ) ) {

		uint8_t dat8;

		if( ignoreerasewhenflash == 0 ){
			fprintf( stderr, "erase chip\n" );
			dat8 = readbyte( &dat8, 0xAC800000 );
			usleep( 30000 );
		}
		
		fprintf( stderr, "write flash memory\n" );
		fprintf( stderr, "adres    : data estim time remaining\n" );
		
		byteswillwrite = rdfff( prgfile, mem );
		
		if( ( modepf == 0x41 ) && (mem->fpagenmb) ){
			byteswillwrite = 0;
			for( adr=0; adr<mem->fpagenmb; ++adr)
				if( mem->f_pages[adr] ) byteswillwrite += mem->fpagesiz;
		}
		
		ndil = 0x10 - 1;
		
		for( writebytes=0, adr=0; adr<mem->fsiz; ){
			uint32_t a;
			
			if(mem->fpagesiz){
				a = adr / mem->fpagesiz;
				if( mem->f_pages[a] == 0 ) {
					adr += mem->fpagesiz;
					continue;
				}
			}
			
			if( modepf == 0x04 ) {
				if( 0 == ( adr & ndil ) )
					fprintf( stderr, "adr 0x%06x: ", adr );
				
				if((mem->f_memory[adr+0] != 0xFF ) ||
				   (mem->f_memory[adr+1] != 0xFF ) ){
					
					fprintf( stderr, "%02x", mem->f_memory[adr+0] );
					fprintf( stderr, "%02x", mem->f_memory[adr+1] );
					a = adr/2;
					a <<= 8;
					a |= 0x40000000 | mem->f_memory[adr+0];
					readbyte( &dat8, a );
					usleep(10000);
					a = adr/2;
					a <<= 8;
					a |= 0x48000000 | mem->f_memory[adr+1];
					readbyte( &dat8, a );
					usleep(10000);
					
					writebytes += 2;
					if( byteswillwrite >= 2 ) byteswillwrite -= 2;
					
				} else {
					fprintf( stderr, "____" );
				}
				adr += 2;
				if( 0 == ( adr & ndil ) ) showtime();
			}
			
			if( modepf == 0x41 ) {
				uint32_t wradr, i;
				
				wradr = adr;
				
				/* write page */
				for( i = 0; i < mem->fpagesiz; i += 2 ){
					
					if( 0 == ( adr & ndil ) ) fprintf( stderr, "adr 0x%06x: ", adr );
					fprintf( stderr, "%02x", mem->f_memory[adr+0] );
					fprintf( stderr, "%02x", mem->f_memory[adr+1] );
					
					a = adr/2;
					a &= (mem->fpagesiz - 1);
					a <<= 8;
					a |= 0x40000000 | mem->f_memory[adr+0];
					readbyte( &dat8, a );
					a = adr/2;
					a &= (mem->fpagesiz - 1);
					a <<= 8;
					a |= 0x48000000 | mem->f_memory[adr+1];
					readbyte( &dat8, a );
					adr += 2;
					
					writebytes += 2;
					if( byteswillwrite >= 2 ) byteswillwrite -= 2;
					if( 0 == ( adr & ndil ) ) showtime();
				}
				
				a = wradr/2;
				a <<= 8;
				readbyte( &dat8, 0x4C000000 | a );
				usleep(10000);
			}

		}
		fprintf( stderr, "\n" );
	}

	/* read eeprom */
	if(( strcmp( memtype, "eeprom" ) == 0 ) &&
		( strcmp( op, "r" ) == 0 ) ) {
		uint8_t dat8, *w8;
			
		fprintf( stderr, "read eeprom memory\n" );
		fprintf( stderr, "adr bytes : data  estimated time remain\n" );

		byteswillwrite = mem->eesiz;
		writebytes = 0;
		ndil = 0x08 - 1;
		w8 = mem->eer_memory;
		while( byteswillwrite ) {
			if( 0 == ( writebytes & ndil ) )
				fprintf( stderr, "adr %06x: ", writebytes );
			readbyte( &dat8, 0xA0000000 | (writebytes<<8) );
			*w8++ = dat8;
			fprintf( stderr, "%02x", dat8 );
			writebytes++;
			
			if( 0 == ( writebytes & ndil ) ) showtime();
			if( 0 == --byteswillwrite ) break;
		}
		fprintf( stderr, "\n" );
		if( format[0] == 'i' )
			write_hex_a ( prgfile, mem->eesiz, mem->eer_memory, 1 );
		if( format[0] == 'r' )
			write_bin_a ( prgfile, mem->eesiz, mem->eer_memory );
	}
	
	/* veryfi eeprom */
	if(( strcmp( memtype, "eeprom" ) == 0 ) &&
		( strcmp( op, "v" ) == 0 ) ) {

		uint8_t dat8, *w8, *ws8;
		
		fprintf( stderr, "veryfi eeprom memory\n" );

		siz = rdeff( prgfile, mem );
		if( siz > (int32_t)mem->eesiz ) siz = mem->eesiz;

		writebytes = 0;
		ndil = 0x08 - 1;
		w8 = mem->eer_memory;
		ws8 = mem->ee_memory;
		
		for( adr=0; adr<mem->eesiz; ) {
			
			if( 0 == ( adr & ndil ) ) fprintf( stderr, "\radr %06x: ", adr );
			
			if( mem->fv_memory[adr] ) {
				readbyte( &dat8, 0xA0000000 | (adr<<8) );
				*w8 = dat8;
				if( dat8 != *ws8 ){
					fprintf( stderr, "bad data read: 0x%02x should be: 0x%02x", dat8, *ws8 );
					fprintf( stderr, " at 0x%06x\n", adr );
					myexit(12);
				}
				++writebytes;
				--siz;
			}
			++adr;
			++ws8;
			++w8;
			
			if( 0 == ( adr & ndil ) ) showtime3();
			if( 0 == siz ) break;
		}
		fprintf( stderr, "\nveryfi OK\n" );
	}
	
	/* write eeprom */
	if(( strcmp( memtype, "eeprom" ) == 0 ) &&
		( strcmp( op, "w" ) == 0 ) ) {

		uint8_t dat8;
		
		fprintf( stderr, "write eeprom memory\n" );
		modepf = 0x04; /* now only one byte */
		ndil = 0x08 - 1;

		byteswillwrite = rdeff( prgfile, mem );
	
		/* if */
		for( writebytes=0; writebytes<mem->eesiz; ) {
			uint32_t a;
			
			if( 0 == ( writebytes & ndil ) ) fprintf( stderr, "adr %06x: ", writebytes );
			
			if( ( modepf == 0x04 ) && ( mem->fv_memory[writebytes] ) ){
				fprintf( stderr, "%02x", mem->ee_memory[writebytes] );
				a = writebytes<<8;
				a |= 0xC0000000 | mem->ee_memory[writebytes];
				readbyte( &dat8, a );
				usleep(10000);
				if( byteswillwrite > 0 ) byteswillwrite -= 1;
			} else {
				fprintf( stderr, "__" );
			}
			writebytes += 1;
			//byteswillwrite = mem->eesiz - writebytes;
			if( 0 == ( writebytes & ndil ) ) showtime();
			if( byteswillwrite == 0 ) break;
		}
		fprintf( stderr, "\n" );
	}
	
	myexit(0);
	return 0;
}


/* -------------------------------------- --------------------------------------




-------------------------------------- -------------------------------------- */

/* ---------------------------------------------------------- */
/*                                                            */
/* ---------------------------------------------------------- */

int wczytajstr ( char * a, char * b, size_t s ) {

	size_t i = 0;
	if(b==NULL) return 0;

	/* skip white */
	while(1) {
		if(*b==0) return 0;
		if(*b>0x20) break;
		++b;
	}
	
	while ( ++i < s ) {
	  if ( *b == 0 ) break;
	  *a++ = *b++;
	}

	*a = 0;

	return i - 1;
}

/* ----------------------------------------- */
/*               sendy                       */
/* ----------------------------------------- */

int slij_32a ( uint32_t * odczyt , uint32_t * outy ) {

	uint32_t zero = 0;
	uint32_t jeden = 1;
	uint32_t dane = *outy;
	uint32_t u = 0x80000000;

	while(u) {
	  *odczyt <<= 1;
	  misSIG ( odczyt );

	  if ( u & dane ) mosSIG(&jeden);
	  else mosSIG(&zero);
		
	  usleep(delaybit);
	  sckSIG(&jeden);
	  usleep(delaybit);
	  sckSIG(&zero);
		
	  u >>= 1;
	}

	usleep(delaybit);
	
	return 0;
}

int slij_32c ( uint32_t * odczyt , uint32_t * outy ) {

	uint32_t zero = 0;
	uint32_t jeden = 1;
	uint32_t * dane = outy;
	uint32_t u = 0x80000000;
	uint32_t tym = 0;

	while( u ) {
	  tym = u & *dane;

		RTS_STAN( fd, &jeden );
		usleep(delaybit);
		
	  *odczyt <<= 1;
	  misSIG ( odczyt );

	  if ( tym == 0 ) mosSIG(&zero);
	  else mosSIG(&jeden);
		
	  usleep(delaybit);
	  sckSIG(&jeden);
	  usleep(delaybit);
	  sckSIG(&zero);
		
	  u >>= 1;
	}

	usleep(delaybit);
	
	return 0;
}

int slij_32b ( uint32_t * odczyt , uint32_t * outy ) {

	uint32_t i = 0;
	uint32_t zero = 0;
	uint32_t jeden = 1;
	uint32_t * dane = outy;
	uint32_t u = 0x80000000;
	uint32_t tym = 0;
	
	for ( i=32; i>0; i-- ) {

	  tym = u & *dane;

	  if ( tym == 0 ) mosSIG(&zero);
	  else mosSIG(&jeden);

	  usleep(delaybit);
	  sckSIG(&jeden);
	  usleep(delaybit);
	  sckSIG(&zero);
		
	  *odczyt <<= 1;
	  misSIG ( odczyt );

	  u >>= 1;
	}

	usleep(delaybit);
	
	return 0;
}

/* ----------------------------------------- */
/*               sety                        */
/* ----------------------------------------- */

/* ustawia bit stan - ktry ustawimy zalezy globalnie */

void RI_STAN ( int fd , uint32_t * stan ) {

  unsigned int ceteel;

  ioctl ( fd, TIOCMGET, &ceteel );
  if ( ceteel & TIOCM_RI ) *stan |= 1;
}

void RI_STAN_NEG ( int fd , uint32_t * stan ) {

  unsigned int ceteel;

  ioctl ( fd, TIOCMGET, &ceteel );
  if ( 0 == (ceteel & TIOCM_RI ) ) *stan |= 1;
}

void CTS_STAN ( int fd , uint32_t * stan ) {

  unsigned int ceteel;

  ioctl ( fd, TIOCMGET, &ceteel );
  if ( ceteel & TIOCM_CTS ) *stan |= 1;
}

void CTS_STAN_NEG ( int fd , uint32_t * stan ) {

  unsigned int ceteel;

  ioctl ( fd, TIOCMGET, &ceteel );
  if ( 0 == (ceteel & TIOCM_CTS ) ) *stan |= 1;
}

void DSR_STAN ( int fd , uint32_t * stan ) {
 
  unsigned int ceteel;

  ioctl ( fd, TIOCMGET, &ceteel );
  if ( ceteel & TIOCM_DSR ) *stan |= 1;
}

static uint32_t PINRTSSTAN = 1;
static uint32_t PINDTRSTAN = 1;

void RTS_STAN ( int fd , uint32_t * stan ) {

  unsigned int ceteel;

  if( PINRTSSTAN == *stan ) return;
  PINRTSSTAN = *stan;

  ioctl ( fd, TIOCMGET, &ceteel );
  if ( *stan ) ceteel |= TIOCM_RTS;
  else ceteel &= ~TIOCM_RTS;
  ioctl ( fd, TIOCMSET, &ceteel );
}

void RTS_STAN_NEG ( int fd , uint32_t * stan ) {

  unsigned int ceteel;

  *stan &= 0x01;
  if( PINRTSSTAN != *stan ) return;

  ioctl ( fd, TIOCMGET, &ceteel );
  if ( *stan ) {
    ceteel &= ~TIOCM_RTS;
    PINRTSSTAN = 0;
  } else {
    ceteel |= TIOCM_RTS;
    PINRTSSTAN = 1;
  }
  ioctl ( fd, TIOCMSET, &ceteel );
}

void DTR_STAN ( int fd , uint32_t * stan ) {

  unsigned int ceteel;

  if( PINDTRSTAN == *stan ) return;
  PINDTRSTAN = *stan;

  ioctl ( fd, TIOCMGET, &ceteel );
  if ( *stan ) ceteel |= TIOCM_DTR;
  else ceteel &= ~TIOCM_DTR;
  ioctl ( fd, TIOCMSET, &ceteel );
}


void DTR_STAN_NEG ( int fd , uint32_t * stan ) {

  unsigned int ceteel;

  *stan &= 0x01;
  if( PINDTRSTAN != *stan ) return;

  ioctl ( fd, TIOCMGET, &ceteel );
  if ( *stan ) {
	  ceteel &= ~TIOCM_DTR;
	  PINDTRSTAN = 0;
  } else {
	  ceteel |= TIOCM_DTR;
	  PINDTRSTAN = 1;
  }
  ioctl ( fd, TIOCMSET, &ceteel );
}


/* funkjc polega na wyslaniu znaku 0x00  czyli chwilowy stan 1 */
/* jesli stan = 0 caly  sygnal       */
/* jesli nie wychodzimy wczesniej    */

void TXD_STAN ( int fd , uint32_t * stan ) {

  int kod_bledu;
  uint8_t znaczek[4];
  znaczek[0] = 0x80;
  if(tryb2x) znaczek[0] = 0xF0; //for 2x fast
  znaczek[1] = 0;
  znaczek[2] = 0;
  znaczek[3] = 0;

  if( * stan == 1 ) {
    kod_bledu = write ( fd, znaczek, 1 );
//    tcflush ( fd, TCOFLUSH ); /* jakis bad */
    if ( kod_bledu == 0 ) fprintf ( stderr, "blad zero\n");
    if ( kod_bledu == -1 ) fprintf ( stderr, "blad minus jeden\n");
  }

}

/* ----------------------------------------- */

void mosSIG ( uint32_t * stan ) {
  wsk_mos ( fd, stan );  
}


void misSIG ( uint32_t * stan ) {
  wsk_mis ( fd, stan );  
}


void rstSIG ( uint32_t * stan ) {
  wsk_rst ( fd, stan );  
}


void sckSIG ( uint32_t * stan ) {
  wsk_sck ( fd, stan );  
}


void pwrSIG ( uint32_t * stan ) {
  wsk_pwr ( fd, stan );  
}

/* ----------------------------------------- */
