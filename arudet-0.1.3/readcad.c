/*
 * Copyright (C) 2020 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>

#include "timeutil.h"

/* -------------------------------- */

extern char desc[512];
extern uint8_t signbyt[3];
extern uint8_t modepf;

extern uint32_t fsiz;
extern uint32_t fpagesiz;
extern uint32_t eepagesiz;
extern uint32_t eesiz;
extern uint32_t fpagenmb;
extern uint32_t eepagenmb;

/* -------------------------------- */

char * getnwathever( char *gdzie, void *c, int max );

enum STR { STNOTHING, STID, STDESC, STSIGNAT, STMEM,
	STSIZ, STMODE, STBLOCK, STFLASH, STEEPROM };

static uint16_t stateSTR;

/* -------------------------------- */

void * findchar( char * str, char type ){
	if( str==NULL ) return NULL;
	while( *str ) if( *++str == type ) return ++str;
	return NULL;
}

/* -------------------------------- */
	
static uint8_t readblock( char *p ){

	unsigned int un, s;
	
	if( NULL == (p = findchar( p, '=' ) ) ) return 0;
	s = sscanf( p, "%u",&un );
	if( s!=1 ) return 1;
	
	if( stateSTR & (1<<STFLASH) ) {
		fpagesiz = un;
		fprintf( stderr, "flash block      : %u\n", un );
	}
	if( stateSTR & (1<<STEEPROM) ) {
		eepagesiz = un;
		fprintf( stderr, "eeprom block     : %u\n", un );
	}
	
	return 0;
}	
	
/* -------------------------------- */

static uint8_t readsiz( char *p ){

	unsigned int un, s;

	if( NULL == (p = findchar( p, '=' ) ) ) return 0;
	s = sscanf( p, "%u",&un );
	if( s!=1 ) return 1;
	
	if( stateSTR & (1<<STFLASH) ) {
		fsiz = un;
		fprintf( stderr, "flash siz        : %u\n", un );
	}
	if( stateSTR & (1<<STEEPROM) ) {
		eesiz = un;
		fprintf( stderr, "eeprom siz       : %u\n", un );
	}
	
	return 0;
}

/* -------------------------------- */

static uint8_t readmem( char *p ){

	char buf[32];
	
	//if( 0 == (stateSTR & (1<<STID) ) ) return 0;

	p = getnwathever( buf, p, sizeof(buf) );
	stateSTR &= ~(1<<STEEPROM);
	stateSTR &= ~(1<<STFLASH);
	if ( !strcmp ( buf, "\"flash\"" ) ) stateSTR |= 1<<STFLASH;
	if ( !strcmp ( buf, "\"eeprom\"" ) ) stateSTR |= 1<<STEEPROM;

	return 0;
}	

/* -------------------------------- */

static uint8_t readmode( char *p ){
	uint16_t s;
	unsigned int un;
	
	if( 0 == (stateSTR & (1<<STFLASH) ) ) return 0;

	if( NULL == (p = findchar( p, '=' ) ) ) return 0;
	s = sscanf( p, "%x",&un );
	if( s!=1 ) return 0;
	modepf = un;
	fprintf( stderr, "mode             : 0x%02x\n", modepf );
	return 0;
}

/* -------------------------------- */

static uint8_t readsig( char *p ){
	uint16_t s;
	unsigned int un[3];
	
	//if( 0 == (stateSTR & (1<<STID) ) ) return 0;

	if( NULL == (p = findchar( p, '=' ) ) ) return 0;
	s = sscanf( p, "%x %x %x",&un[0], &un[1], &un[2] );
	if( s!=3 ) return 1;
	signbyt[0] = un[0];
	signbyt[1] = un[1];
	signbyt[2] = un[2];
	fprintf( stderr, "signature        : " );
	fprintf( stderr, "%02x %02x %02x\n", signbyt[0], signbyt[1], signbyt[2] );
	return 0;
}

/* -------------------------------- */

static uint8_t readdesc( char *p ){
	uint16_t s;
	
	//if( 0 == (stateSTR & (1<<STID) ) ) return 0;

	if( stateSTR & (1<<STDESC) ) {
		//fprintf( stderr, "again ?!?!?!\n" );
		return 1;
	}
	if( NULL == (p = findchar( p, '"' ) ) ) return 0;
	
	for( s=0; s<sizeof(desc)-1; ) {
		desc[s++] = *p;
		if( *++p < '$' ) break;
	}
	desc[s] = 0;
	fprintf( stderr, "description      : %s\n", desc );
	stateSTR |= 1<<STDESC;
	return 0;
}

/* -------------------------------- */

static uint8_t readid( char *p , char *chips ){
	
	char * b = chips;

	if( stateSTR & (1<<STID) ) return 1;
	if( NULL == ( p = findchar( p, '"' ) ) ) return 0;
	
	for( ; ; ++chips, ++p ) {
		if( *p == *chips ) continue;
		if( (*p<'$') && ( *chips == 0 ) ) break;
		return 0;
	}
	fprintf( stderr, "find avrdudeconf : %s\n", b );
	stateSTR |= 1<<STID;
	return 0;
}

/* -------------------------------- */

uint8_t readchpfcad( char * pathcavrd, char *chips ) {
	
	FILE * fconfavrdude;
	int r;
	char chip[128];
	char * line;
	size_t linelength;
	
	if( chips == NULL ) return 1;
	if( chips[0] == 0 ) return 1;
	
	if( pathcavrd == NULL )
		fconfavrdude = fopen( "/etc/avrdude.conf", "r" );
	else 
		fconfavrdude = fopen( pathcavrd , "r" );
	
	if( NULL == fconfavrdude ) return 1;
	
	fsiz = 0;
	fpagesiz = 0;
	eepagesiz = 0;
	eesiz = 0;
	fpagesiz = 0;
	eepagesiz = 0;
	
	stateSTR = 0;
	linelength = 512;
	line = malloc(linelength);
	if(line==NULL) return 1;
	
	while(1){
		
		char * p;
		r = getline( &line, &linelength, fconfavrdude);
		if( r < 0 ) break;
		
		p = getnwathever( chip, line, sizeof(chip) );
		if( p == NULL ) continue;

		if( !strcmp( chip, "id" )){ if( readid( p, chips )) break; }
		if( 0 == (stateSTR & (1<<STID) ) ) continue;
		
		if( !strcmp( chip, "desc" ) ){ if( readdesc( p )) return 1; }
		else if( !strcmp( chip, "signature" )){ if( readsig( p )) return 1; }
		else if( !strcmp( chip, "memory" )){ if( readmem( p )) return 1; }
		else if( !strcmp( chip, "size" )){ if( readsiz( p )) return 1; }
		else if( !strcmp( chip, "mode" )){ if( readmode( p )) return 1; }
		else if( !strcmp( chip, "blocksize" )){ if( readblock( p )) return 1; }
	}

	if( modepf == 0x04 ){
		fpagesiz = 0;
		fpagenmb = 0;
	} else {
		if(fpagesiz) fpagenmb = fsiz/fpagesiz;
	}
	if( eepagesiz != eesiz ) {
		if(eepagesiz) eepagenmb = eesiz/eepagesiz;
	} else {
		eepagesiz = 0;
		eepagenmb = 0;
	}
	
	fprintf( stderr, "eepagenmb        : %u\n", eepagenmb );
	fprintf( stderr, "fpagenmb         : %u\n", fpagenmb );
	fprintf( stderr, "--------------------------------------\n");
	free( line );
	fclose( fconfavrdude );
	return 0;
}

/* ---------------------------------------------------------- */
/*                                                            */
/*         find something and copy                            */
/*                                                            */
/* ---------------------------------------------------------- */

char * getnwathever( char *gdzie, void *c, int max ) {
  uint16_t ile = 0;
  unsigned char *co = c;
  
  if(co==NULL) return NULL;
  if(max<2) return NULL;
  
  gdzie[0] = 0;
  
  while(1) {
    if(*co==0) return NULL;
    if(*co>0x20) break;
    ++co;
  }
  
  while(1) {
    *gdzie = *co;
    if (*co<33) break;
    if( ++ile == max ) {
      *gdzie = 0;
      return NULL;
    };
    ++gdzie;
    ++co;
  }

  *gdzie = 0;
  
  return (char*) co;
}
