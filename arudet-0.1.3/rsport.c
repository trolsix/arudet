/*
 * Copyright (C) 2020 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>

#include <time.h>

#include "rsport.h"
#include "timeutil.h"


void * WCZAS;
float CZAS;
unsigned int opuznienie;

/* zwraca numer */
int stanport ( char * dev, int co, int num, int speed ) {

	static struct termios mode, oldmode;
	//static speed_t VP1, VP2;
	int fd = -1;
	
	if ( co == 3 ) {
	//if (( fd = open(dev, O_RDWR | O_NONBLOCK )) < 0) {
		if (( fd = open(dev, O_RDWR )) < 0) {
		  perror(dev);
		  fprintf( stderr, "not open %s \n", dev);
		  return(-1);
		 }
	//  fprintf( stderr, "open %s  num %i\n", dev, fd);

	  if ( tcgetattr( fd, &mode) ) {
		 perror(dev);
		 close(fd);
		 return -1;
		}

	  memcpy( &oldmode, &mode, sizeof(oldmode) );

		//VP1 = cfgetispeed( &mode );
	  //VP2 = cfgetospeed( &mode );

	mode.c_iflag = IMAXBEL;
	mode.c_cflag = CREAD | CS8 | HUPCL | CLOCAL;
	mode.c_lflag = IEXTEN;
	mode.c_oflag = OPOST;
	
		/*
	  fprintf( stderr, "i %04u ", mode.c_iflag );
	  fprintf( stderr, "c %04u ", mode.c_cflag );
	  fprintf( stderr, "l %04u ", mode.c_lflag );
	  fprintf( stderr, "o %04u\n", mode.c_oflag );*/
		
		//dla trybu blok
		mode.c_cc [VMIN] = 0;
		mode.c_cc [VTIME] = 0;

		if ( speed != 19200 ) {
		  cfsetispeed ( &mode, B19200 );
		  cfsetospeed ( &mode, B19200 );
		  tcsetattr ( fd, TCSANOW, &mode );
		  usleep(10000);
		}
		if ( speed == 9600 ) {
		  cfsetispeed ( &mode, B9600 );
		  cfsetospeed ( &mode, B9600 );
		}
		if ( speed == 4800 ) {
		  cfsetispeed ( &mode, B4800 );
		  cfsetospeed ( &mode, B4800 );
		}
		if ( speed == 600 ) {
		  cfsetispeed ( &mode, B600 );
		  cfsetospeed ( &mode, B600 );
		}
		if ( speed == 300 ) {
		  cfsetispeed ( &mode, B300 );
		  cfsetospeed ( &mode, B300 );
		}
		if ( speed == 1200 ) {
		  cfsetispeed ( &mode, B1200 );
		  cfsetospeed ( &mode, B1200 );
		}
		if ( speed == 2400 ) {
		  cfsetispeed ( &mode, B2400 );
		  cfsetospeed ( &mode, B2400 );
		}
		if ( speed == 19200 ) {
		  cfsetispeed ( &mode, B9600 );
		  cfsetospeed ( &mode, B9600 );
		  tcsetattr ( fd, TCSANOW, &mode );
		  usleep(5000);
		  cfsetispeed ( &mode, B19200 );
		  cfsetospeed ( &mode, B19200 );
		}
		if ( speed == 38400 ) {
		  cfsetispeed ( &mode, B38400 );
		  cfsetospeed ( &mode, B38400 );
		}
		if ( speed == 57600 ) {
		  cfsetispeed ( &mode, B57600 );
		  cfsetospeed ( &mode, B57600 );
		}
		if ( 115200 == speed ) {   
		  cfsetispeed ( &mode, B115200 );
		  cfsetospeed ( &mode, B115200 );
		}
		if ( speed == 230400 ) {
		  cfsetispeed ( &mode, B230400 );
		  cfsetospeed ( &mode, B230400 );
		}
		if ( speed == 500000 ) {
		  cfsetispeed ( &mode, B500000 );
		  cfsetospeed ( &mode, B500000 );
		}
		if ( speed == 1000000 ) {
		  cfsetispeed ( &mode, B1000000 );
		  cfsetospeed ( &mode, B1000000 );
		}

	  //VP1 = cfgetospeed( &mode );
	  //fprintf( stderr, "before %04u after %04u\n", VP2, VP1 );
		 
	  tcsetattr ( fd, TCSANOW, &mode );
	  usleep(5000);

	  tcflush ( fd, TCIFLUSH );
	  tcflush ( fd, TCOFLUSH );
	  usleep(5000);
	  
	  return fd;
	} 

	if ( co == 4 ) {

	  usleep(20000);
	  tcsetattr ( num, TCSANOW, &oldmode );
	  usleep(10000);
	  close(num);
	  //if ( PORT_INF ) 
		 //fprintf( stderr, "close %s  num %i\n", dev, num );
		 fprintf( stderr, "close %s\n", dev);

	  return 0;
	}

	return -1;
}

/*
TCSADRAIN
TCSAFLUSH
*/
