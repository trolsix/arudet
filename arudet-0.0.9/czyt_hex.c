/*
 * Copyright (C) 2020 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
#include <pthread.h>
#include <signal.h>
#include <semaphore.h>
*/

#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
//#include <linux/serial.h>

#include "czyt_hex.h"

/*-----------------------------------------
      CRC 8
 */
unsigned char CRC_8 ( void * d, int i ) {

    unsigned char *din, c;
    din = d;
    c = 0;

    while ( i-- )
        c += *din++;
    
return c;
}

/*-----------------------------------------
      linia hex na bin
 */
void gethexline ( void * d, void * s, int i ) {
    
unsigned char *de , *so;

    i /= 2;
    de = d;
    so = s;
    
   while (i--) {
      *de++ = hextochar ( (void*) so );
       so += 2;
   }

}

/*-----------------------------------------
      read bin
 */

uint32_t read_bin_a ( FILE * plik,
                uint32_t iled ,
                uint8_t * MEM ,
                uint32_t startadr,
                uint8_t * pageset,
                uint16_t pagesiz
) {
	uint32_t i, j;
	
	if ( NULL == plik ) return 0;
	
	if( startadr ) {
		i = fseek( plik, startadr, SEEK_SET );
		if ( i ) {
			fprintf( stderr, "Bad bin: read from %u adr\n", startadr );
			return 0;
		}
	}
		
	i = fread( MEM, 1, iled, plik );
	
	if( pagesiz == 0 ) return i;
		
	for( j=0; j<i; ++j, ++MEM )
		if( *MEM != 0xFF ) pageset[j/pagesiz] = 1;

	return i;
}

/*-----------------------------------------
      write bin
 */

int write_bin_a ( FILE * plik, uint32_t iled , uint8_t * MEM ) {
	
	uint32_t i;
	
	i = fwrite( MEM, 1, iled, plik );
	
	if( i != iled ) {
		fprintf( stderr, "Bad: write %u bytes from %u\n", i, iled );
	}
	
	return i;
}

/*-----------------------------------------
      read hex
 */

uint32_t read_hex_c ( FILE * plik,
                uint32_t iled ,
                uint8_t * MEM ,
                uint32_t startadr,
                uint8_t * pageset,
                uint16_t pagesiz
) {

	char *bufor;
	uint8_t ADR_LOW, ADR_HIGH;
	uint8_t ILE, TYP, CRC, CRCO;
	uint32_t ILES, ILET, ilenff;
	uint32_t a;
	size_t sizb = 1024; //na linie
	uint8_t *bufbin, *wbufbin;
	uint32_t endadr;

	endadr = startadr + iled;

	bufor = malloc (sizb); //duzy bufor na linie
   bufbin = malloc (sizb); //duzy bufor na linie bin

	if ( NULL == plik ) return 0;

	for (a=0; a<iled; a++) MEM[a] = 0xFF;
	
	ILES = 0;
	ilenff = 0;

	while (1) {
		unsigned int ile;
		ile = getline ( &bufor, &sizb, plik);

		if ( ile <= 0 ) {
			fclose ( plik );
			plik = NULL;
			break;
		}
    
		ILE = hextochar ( &bufor[1] );
       if ( ILE > ((ile-1)/2) ) return -2;
       if ( bufor[0] != ':' ) return -3;

       gethexline ( bufbin, &bufor[1], ile-1 );
       
		ADR_HIGH = bufbin[1];
		ADR_LOW = bufbin[2];
		TYP = bufbin[3];
		CRC = CRC_8 ( bufbin, ILE + 4 );
		
         CRC = 0xFF - CRC;
			CRC += 1;
         CRCO = bufbin[ILE+4];
			if ( CRCO != CRC ) {
				fprintf( stderr, " BAD CRCO %02x CRC %02x\n", CRCO, CRC );
				return -1;
			}

          //hex adr 32
		if ( TYP == 4 ) {
         //ILES &= 0x0000FFFF;
			ILET = bufbin[4];
			ILET <<= 8;
			ILET += bufbin[5];
         ILET <<= 16;
         ILES |= ILET;
         continue;
      }
          
		if ( TYP == 0 ) {
         ILES &= 0xFFFF0000;
			ILET = ADR_HIGH;
			ILET <<= 8;
			ILET += ADR_LOW;
         ILES |= ILET;
          
			//tym = 4;
			wbufbin = bufbin + 4;

	      while ( ILE-- ) {
             if ( ILES >= endadr ) break;
				a = ILES / pagesiz;
				if(pageset) pageset[a] = 1;
    
             if ( ILES >= startadr ) {
                 MEM[ILES-startadr] = *wbufbin++;
                 ++ilenff;
				 }
             ILES++;
			}

		}
		if ( TYP == 1 )
			break;
	}

  if ( plik != NULL ) {
    fclose ( plik );
    plik = NULL;
  }
	//fprintf( stderr, "max adr: %08x %u\n", ILES , ILES );

  free (bufor);
  free (bufbin);
  
  return ilenff;
}

/*-----------------------------------------
      check file
 */

FILE * fileexistsopen( const char * filename ) {
	FILE *file;
	
	file = fopen(filename, "r");
	if ( file ){
		fclose(file);
		fprintf( stderr, "file %s exist overwrite\n", filename );
		return fopen(filename, "w");
	}
	
	return fopen(filename, "w");
	 /*
	 struct stat buffer;
    int exist = stat(filename,&buffer);
    if(exist == 0)
        return 1;
    else // -1
		return 0;*/
}

/*-----------------------------------------
      write to hex file
 */

int write_hex_a ( FILE * plik, uint32_t iled , uint8_t * MEM ) {

	uint32_t adr32, sizbyt;
	uint8_t CRC, i;

	adr32 = 0;
	sizbyt = 16;
	
	if( plik == NULL ) return -1;
	
	/* from adr 0 */
	fprintf( plik, ":020000020000FC\n" );
	
	for( adr32=0; iled >= sizbyt; iled-=sizbyt, adr32+=sizbyt ) {
		
		for(i=0;i<sizbyt;++i) if( MEM[i] != 0xFF ) break;
		if( i >= sizbyt ) continue;
		
		CRC = 0;
		CRC -= sizbyt;
		CRC -= adr32 & 0xFF;
		CRC -= (adr32>>8) & 0xFF;
		fprintf( plik, ":%02X", sizbyt );
		fprintf( plik, "%04X", adr32 & 0xFFFF );
		fprintf( plik, "%02X", 0x00 );
		for(i=0;i<sizbyt;++i){
			fprintf( plik, "%02X", *MEM );
			CRC -= *MEM++;
		}
		fprintf( plik, "%02X\n", CRC );
	}
	
	while( iled ) {
		for(i=0;i<iled;++i) if( MEM[i] != 0xFF ) break;
		if( i >= iled ) break;
		CRC = 0;
		CRC -= iled;
		CRC -= adr32 & 0xFF;
		CRC -= (adr32>>8) & 0xFF;
		fprintf( plik, ":%02X", iled );
		fprintf( plik, "%04X", adr32 & 0xFFFF );
		fprintf( plik, "%02X", 0x00 );
		for(i=0;i<iled;++i){
			fprintf( plik, "%02X", *MEM );
			CRC -= *MEM++;
		}
		fprintf( plik, "%02X\n", CRC );
		break;
	}
	
	fprintf( plik, ":00000001FF\n" );
	
	return 0;
}

/*-----------------------------------------

 */
/*
: ile adr_h adr_l typ ile_x_2  CRC
: 00 00 00 00 000000000 

typ:
00 dane
01 koniec

CRC = sum mod 256= 0 czyli 256 - suma = CRC 
*/

/*-----------------------------------------
      hex to char
 */

unsigned char hextochar ( char * ws ) {

  unsigned char b;

  b = 0;

    if ( ( *ws >= '0' ) && ( *ws <= '9' ) ) b = (*ws) - '0';
    else if ( ( *ws >= 'a' ) && ( *ws <= 'f' ) ) b = ((*ws) - 'a')+10;
    else if ( ( *ws >= 'A' ) && ( *ws <= 'F' ) ) b = ((*ws) - 'A')+10;

    b = b << 4;
    ws++; 

    if ( ( *ws >= '0' ) && ( *ws <= '9' ) ) b |= (*ws) - '0';
    else if ( ( *ws >= 'a' ) && ( *ws <= 'f' ) ) b |= ((*ws) - 'a')+10;
    else if ( ( *ws >= 'A' ) && ( *ws <= 'F' ) ) b |= ((*ws) - 'A')+10;

  return b;
}

/* ----------------------------------------- */
/*               end file                    */
/* ----------------------------------------- */
