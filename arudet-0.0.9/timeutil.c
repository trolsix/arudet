/*
 * Copyright (C) 2020 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>

#include "timeutil.h"

static struct timeval start;
static struct timeval stop;


void starttimer (void) {
	gettimeofday( &start, 0 );
}


uint32_t gettimems (void) {
	uint64_t t1,t2;
	gettimeofday( &stop, NULL );
	t2 = stop.tv_sec * 1000000;
	t2 += stop.tv_usec;
	t1 = start.tv_sec * 1000000;
	t1 += start.tv_usec;
	t2 -= t1;
	t2 /= 1000; /* ms */
	return t2;
}

