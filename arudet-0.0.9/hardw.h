/*
 * Copyright (C) 2020 Tomasz C. aka trol.six (elektroda.pl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
#ifndef _hardw_h
#define _hardw_h

/*
  
  UART1
  TxD -> not -> RST
  DTR -> SCK
  RTS -> MISO
  CTS -> MOSI

  UART2
  TxD -> not -> SCK
  DTR -> RST
  RTS -> MISO
  CTS -> MOSI

  not for UART
  
                  V.uk
                 _|_
                |10k|
                |   |
                |___|
                  |
                  |_____ out
  in   _____    | /
  ____| 68K |___|/
       -----    |\
                |_\|____ gnd



  gate not for RS232
  
                       V.uk
                      _|_
                     |10k|
                     |   |
                     |___|
                       |
                       |_____ out
  in  _____          | /
  ___| 68K |_________|/
      -----   |      |\
              |      |_\|
              |         |
              |__|/|____|____ gnd
                 |\|



____    ____
    \  /
   ------
     |
     |

*/

/* ------------------------------------------------ */

uint8_t modepf;
uint8_t signbyt[3];
uint32_t baudrateH;

char wheren[1024];
char hardwn[1024];
char elementn[512];
char akcjan[512];
char desc[256];

/* ------------------------------------------------ */


/* ------------------------------------------------ */

#endif
